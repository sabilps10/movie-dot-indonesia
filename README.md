*PENJELASAN APLIKASI*

1. Aplikasi menggunakan react native expo karena membuat aplikasi yang simple.
2. Navigation menggunakan react-navigation karena common dan lite.
3. Network call menggunakan axios.
4. Styling menggunakan tailwind karena sangat costumeable.
5. Carousel menggunakan react-native-snap-carousel

*MENJALANKAN APLIKASI*

1. git clone https://gitlab.com/sabilps10/movie-dot-indonesia.git
2. yarn install untuk menginstall package
3. npm run android / ios untuk menjalankan aplikasi